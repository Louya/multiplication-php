<!DOCTYPE html>
<html lang="fr">

<head>
    <!-- Meta obligatoires -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Meta optionnels -->
    <meta name="author" content="Fabien Cazalet">

    <!-- Lien Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
        crossorigin="anonymous">
    <!-- lien CSS -->
    <link rel="stylesheet" href="style.css">
    <link href="https://fonts.googleapis.com/css?family=Fjalla+One|Roboto" rel="stylesheet">

    <title>Calculatrix</title>
</head>

<body>
    <header class="container-fluid py-2">
        <div class="container d-flex flex-column align-items-center py-3">
            <h1><span class="coloredLetter">C</span>AL<span class="coloredLetter">C</span>ULATRIX</h1>
            <p>Révise tes tables de multiplications facilement</p>
        </div>
    </header>
    <main class='container-fluid py-2'>
        <section class="container">
            <div class="row px-5">
                <p class="intro">
                    Bienvenue sur Calculatrix, le site qui te permettra de réviser tes tables de multiplications
                    facilement.
                    Tu peux afficher les tables que tu souhaite réviser puis t'entrainer avec le mode révision. Bonne
                    chance !
                </p>
            </div>
        </section>
        <section class="selection container d-flex justify-content-center">
            
                <form action="index.php" method="post" class="row">
                    <!-- choix des tables de multiplicatons disponibles -->
                    <div class="tablesDispo p-0 col-9 d-flex">
                        <?php //génération du fomulaire
                        for ($i=1; $i <= 10; $i++){
                            echo '<div class="table'.$i.' ">';
                            echo '<label class="px-1" for="table'.$i.'">';
                            echo '<input class="px-1" id="table'.$i.'" name="table[]" value="'.$i.'" type ="checkbox">';
                            echo $i.'</label>';
                            echo '</div>';
                        }
                    ?>
                    </div>
                    <div class="col-3 p-0 d-flex justify-content-end align-items-center">
                        <!-- Bouton d'affichage des tables de multiplication -->
                        <input type="submit" name="choice" value="A" class="boutonTab">
                    </div>
                </form>
          
        </section>
        <section class="tablesVisu container d-flex flex-wrap justify-content-lg-center px-0 py-2">
            <?php
                // déclaration des tables de multiplications
                function createTable ($n){
                    for ($j=1; $j <= 10; $j++){
                        echo '<div class="d-flex justify-content-between">';
                        echo '<p class="m-0">'.$n." x ".$j.' = </p><span class="text-right result">'.$n*$j.'</span>';
                        echo '</div>';
                    }
                }
                // Affichage des tables selectionnées 
                $choice = $_POST['choice'];
                if (isset($choice)) { //test quand on clic sur le bouton
                    foreach($_POST['table'] as $i){
                        echo '<div class="cadre mx-2 p-3">';
                        echo '<h3><span class="coloredLetter">t</span>able de <span class="coloredLetter">'.$i.'</span></h3>';
                        createTable($i);
                        echo '</div>';
                    }
                }                
            ?>
        </section>
        <section class="revision container d-flex flex-column align-items-center py-3">
            <h2>Mode super révision</h2>
            <div class="question d-flex flex-column align-items-center">
                <form action="index.php" id="revision" method="post">
                    <span>Révise la table de </span>
                    <?php
                        // Variables du mode révision
                        $message = 'Essaie de trouver la réponse';
                        $answer = $_POST['answer'];
                        // fonction de création de selection de table de multiplication
                        function tableRevision($k){
                            echo '<select name="n1">';
                            for ($i = 1; $i <= 10; $i++){
                                if ($i == $k){ echo '<option value="'.$i.'" selected="selected">'.$i.'</option>';} 
                                else {echo '<option value="'.$i.'">'.$i.'</option>';}
                            }
                            echo '</select>';
                        }
                        function emptyAnswer($x){
                            $n2 = rand(1,10);
                            tableRevision($x);
                            echo ' x ';
                            echo   '<input type="text" name="n2" value="'.$n2.'" readonly>' ;
                        }
                        //test quand on clic sur le bouton
                        if (isset($_POST['answer'])){
                            if ($answer !== ''){ // test du champ de réponse
                                $n1 = $_POST['n1'];
                                $n2 = $_POST['n2'];
                                $goodAnswer = $n1*$n2; 
                                tableRevision($n1); // creation de la sélection
                                echo ' x ';
                                echo   '<input type="text" name="n2" value="'.$n2.'" readonly>'; // creation ou actualisation de la valeur 2
                                if($answer == $goodAnswer){
                                    $message = "C'est gagné ! la réponse est bien ".$goodAnswer;
                                } else {
                                    $message = "C'est perdu, essaie encore";
                                }
                            } 
                            else {
                                $n1 = $_POST['n1']; 
                                emptyAnswer($n1);
                            }
                        } else {
                            $n1 = 1;
                            emptyAnswer($n1);
                        }
                    ?>
                    <input type="text" name="answer" autofocus>
                    <input type="submit" name="reponse" value="=" class="boutonRev">
                </form>
            </div>
            <div class="reponse">
                <?php echo $message; ?>
            </div>
            <!-- <div class="d-flex justify-content-center">
                <button class="btn btn-warning" onclick="sendRequest()" id="bouton">AJAX C'EST LA VIE !!!!!!!!!!!!!!!</button>
            </div>
            <div id="demo">
            </div> -->
        </section>
    </main>
    <footer class="container-fluid">
        <div class="container">

        </div>
    </footer>

    <!-- Script bootstrap -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>

    <!-- liens javascript -->
    <script src="app.js"></script>
    <!-- script AJAX -->
    <script src="ajax.js"></script>
</body>

</html>